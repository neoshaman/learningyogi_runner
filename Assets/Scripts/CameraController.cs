﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	#region Attributes
	public Transform player;
	public float yDamping;
	#endregion

	#region Private Variables
	Vector3 offset;
	#endregion

	#region MonoBehaviour
	private void Start()
	{
		offset = transform.position - player.position;
	}

	private void LateUpdate()
	{
		float y = Mathf.Lerp(transform.position.y, player.position.y, yDamping * Time.deltaTime);
		transform.position = new Vector3(player.position.x + offset.x, y, transform.position.z);
	}
	#endregion
}
