﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	#region Attibutes
	public AudioClip gameOverClip;
	public AudioClip jumpClip;
	public AudioClip cherryClip;
	#endregion


	#region Private Variables
	private AudioSource audioSource;
	#endregion

	#region MonoBehaviour
	private void Awake()
	{
		audioSource = GetComponent<AudioSource>();
	}
	#endregion

	#region Actions
	public void GameOver()
	{
		audioSource.clip = gameOverClip;
		audioSource.Play();
		Debug.Log("play game over");

	}

	public void Jump()
	{
		audioSource.clip = jumpClip;
		audioSource.Play();
	}

	public void Cherry()
	{
		Debug.Log("play cherry");
		audioSource.clip = cherryClip;
		audioSource.Play();
	}
	#endregion
}
