﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	#region Attributes
	public GameObject gamePanel;
	public GameObject gameOverPanel;
	public Text scoreText;
	public Text gameOverScoreText;
	public Text highscoreScoreText;
	#endregion

	#region Private
	private float score;
	private float highscore;
	#endregion

	#region MonoBehaviour
	private void Awake()
	{
		highscore = PlayerPrefs.GetFloat("highscore");
	}
	#endregion


	#region Actions
	public void AddToScore (float points) {
		score += points;
		if (score > highscore)
			highscore = score;
		scoreText.text = ((int)score).ToString();
	}

	public void GameOver()
	{
		gameOverScoreText.text = ((int)score).ToString();
		highscoreScoreText.text = ((int)highscore).ToString();
		gamePanel.SetActive(false);
		gameOverPanel.SetActive(true);
		PlayerPrefs.SetFloat("highscore", highscore);
	}

	public void Replay()
	{
		SceneManager.LoadScene(0);
	}
	#endregion
}
