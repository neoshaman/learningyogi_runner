﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	#region Attributes
	public Transform groundCheck;
	public LayerMask groundLayerMask;
	public GameManager gameManager;
	public SoundManager soundManager;
	public float runSpeed;
	public float jumpForce;
	public float isGroundedDistance;
	public float groundOffset;
	public float groundTolerance;
	public float scoreMultiplier;
	public float cherryPoints;
	#endregion

	#region Private Members
	private bool grounded;
	private Animator animator;
    private Rigidbody2D rb;
	private bool triggerJump;
	private bool jumping;
	private bool falling;
	private float groundDistance;
	readonly private int GROUNDING_DELAY_FRAMES = 5;
	private int jumpingFrames;
	private float score;
	#endregion

	#region Properties
	public bool IsJumping
	{
		get { return jumping;  }
	}
	#endregion

	#region Actions
	public void SetJump(bool jump)
	{
		triggerJump = jump;
	}

	public void Die()
	{
		animator.SetBool("Dead", true);
		StopCoroutine("Run");
		rb.isKinematic = true;
		rb.velocity = Vector2.zero;
		gameManager.GameOver();
		soundManager.GameOver();
	}
	#endregion

	#region Monobehaviour
	private void Awake()
	{
		animator = GetComponent<Animator>();
		rb = GetComponent<Rigidbody2D>();
	}

	private void Start()
	{
		StartCoroutine("Run");
	}

	private void FixedUpdate()
	{
		grounded = false;
		RaycastHit2D hit = Physics2D.Raycast(groundCheck.position, Vector2.down, Mathf.Infinity, groundLayerMask, -Mathf.Infinity, Mathf.Infinity);
		if (hit)
		{
			groundDistance = hit.distance;
			if (groundDistance < isGroundedDistance)
			{
				if (!jumping || (jumpingFrames > GROUNDING_DELAY_FRAMES && rb.velocity.y < 0))
				{
					//if (jumping)
					grounded = true;
					jumping = false;
				}
			}
			if (!jumping)
			{
				if (groundDistance > groundTolerance)
				{
					grounded = true;
					transform.position = new Vector3(transform.position.x, transform.position.y - groundDistance + groundOffset, transform.position.z); //keep player on the ground when going over slopes
				}
			}
			else
				jumpingFrames++;
		}

		float yVelocity = grounded ? 0 : rb.velocity.y;
		animator.SetFloat("yVelocity", yVelocity);
		animator.SetBool("Grounded", grounded);

		gameManager.AddToScore(scoreMultiplier * Time.deltaTime);
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		bool obstactle = collider.gameObject.CompareTag("Obstacle");
		if (obstactle)
		{
			Debug.Log("obst");

			Die();
		}
		else
		{
			Destroy(collider.gameObject);
			gameManager.AddToScore(cherryPoints);
			soundManager.Cherry();
			Debug.Log("cherry");
		}
	}
	#endregion

	#region Private Methods
	private IEnumerator Run()
	{
		while (true)
		{
			Move();
			yield return null;
		}
	}

	private void Move()
	{
		if (grounded)
		{
			//animator.SetFloat("Speed", Mathf.Abs(runSpeed));
			//transform.position = new Vector3(transform.position.x, groundHeight, transform.position.z);	//keep player on the ground when going over slopes
			if (!jumping)
				rb.velocity = new Vector2(runSpeed, rb.velocity.y);
			if (jumping && grounded)
			{
				jumping = false;
			}
			if (triggerJump)
			{
				grounded = false;
				jumping = true;
				rb.AddForce(new Vector2(0f, jumpForce));
				jumpingFrames = 0;
				soundManager.Jump();
			}
		}
	}
#endregion
}
