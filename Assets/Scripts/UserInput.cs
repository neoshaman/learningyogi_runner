﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInput : MonoBehaviour {

	#region Private Variables
	private PlayerController playerController;
	#endregion

	#region MonoBehaviour
	private void Awake()
	{
		playerController = GetComponent<PlayerController>();
	}

	private void Update()
	{
		bool jump;
#if UNITY_EDITOR
		jump = Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0);
#else
		jump = (Input.touchCount > 0);
#endif
		playerController.SetJump(jump);
	}
	#endregion

}
