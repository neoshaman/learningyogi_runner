﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainSpawner : MonoBehaviour {

	public enum TerrainType {FLAT, UPSLOPE, DOWNSLOPE };

	#region Attributes
	public GameObject flatGroundPrefab;
	public GameObject slopeUpPrefab;
	public GameObject slopeDownPrefab;
	public GameObject groundFillPrefab;
	public GameObject spikePrefab;
	public GameObject skullSpikePrefab;
	public GameObject cherryPrefab;
	[Header("Terrain")]
	public float positionScale = 1f;
	public float flatGroundMean;
	public float flatGroundDeviation;
	public float slopeMean;
	public float slopeDeviation;
	public float camSpawnOffset;
	[Header("Spikes")]
	public float spikeProbability;
	public float maxSpikeProbablity;
	public float spikeProbablityIncrease;
	public float skullProbability;
	public float minSpikeCloseness;
	[Header("Cherries")]
	public float cherryProbability;
	public float minCherryHeight;
	public float maxCherryHeight;
	#endregion

	#region Private Variables
	private float currentHeight;
	private float currentDistance;
	private Camera cam;
	private float lastSpikeX;
	readonly private float SPIKE_ROTATION = 24f;
	readonly private float UPSLOPE_SPIKE_Y = .14f;
	readonly private float DOWNSLOPE_SPIKE_Y = 0.66f;
	#endregion

	#region MonoBehaviour
	private void Awake()
	{
		cam = Camera.main;
	}

	private void Start() {
		GenerateTerrain();
		//GenerateTerrain();
		//GenerateTerrain();
	}

	private void Update()
	{
		if (cam.transform.position.x + camSpawnOffset > currentDistance)
		{
			spikeProbability = Mathf.Min(maxSpikeProbablity, spikeProbability + spikeProbablityIncrease);

			//Debug.Log("new spikeProbability: " + spikeProbability);
			GenerateTerrain();
		}
	}
	#endregion


	#region Private Methods
	private void GenerateTerrain()
	{
		TerrainType terrainType = (TerrainType)Random.Range(0, 3);
		switch (terrainType)
		{
		case TerrainType.FLAT:
			CreateFlatGround((int)Distrubution(flatGroundMean, flatGroundDeviation));
			break;
		case TerrainType.UPSLOPE:
			CreateUpslope((int)Distrubution(slopeMean, slopeDeviation));
			break;
		case TerrainType.DOWNSLOPE:
			CreateDownslope((int)Distrubution(slopeMean, slopeDeviation));
			break;
		}

	}

	private void CreateFlatGround(int units)
	{
		for (int i = 0; i < units; i++)
		{
			GameObject newTerrain = Instantiate(flatGroundPrefab, transform.position + new Vector3(currentDistance * positionScale, currentHeight * positionScale, 0),
				Quaternion.identity, transform);
				/*GameObject newTerrain = */
				Instantiate(groundFillPrefab, transform.position + new Vector3(currentDistance * positionScale, (currentHeight - .5f) * positionScale, 0),
					Quaternion.identity, newTerrain.transform);
			currentDistance++;
			MaybeAddSpike(newTerrain.transform, TerrainType.FLAT);
			MaybeAddCherry(newTerrain.transform);
		}
	}

	private void CreateUpslope(int units)
	{
		for (int i = 0; i < units; i++)
		{
			
			currentHeight++;
			GameObject newTerrain = Instantiate(slopeUpPrefab, transform.position + new Vector3(currentDistance * positionScale, currentHeight * positionScale, 0),
				Quaternion.identity, transform);
			for (int j = 0; j < 2; j++)
				Instantiate(groundFillPrefab, transform.position + new Vector3((currentDistance + j) * positionScale, (currentHeight + j - 1) * positionScale, 0),
					Quaternion.identity, newTerrain.transform);
			currentDistance += 2;
			MaybeAddSpike(newTerrain.transform, TerrainType.UPSLOPE);
			MaybeAddCherry(newTerrain.transform);
		}
	}

	private void CreateDownslope(int units)
	{
		for (int i = 0; i < units; i++)
		{
			GameObject newTerrain = Instantiate(slopeDownPrefab, transform.position + new Vector3(currentDistance * positionScale, currentHeight * positionScale, 0),
				Quaternion.identity, transform);
			for (int j = 0; j < 2; j++)
				Instantiate(groundFillPrefab, transform.position + new Vector3((currentDistance + j) * positionScale, (currentHeight - j * .5f - 1) * positionScale, 0),
					Quaternion.identity, newTerrain.transform);
			currentDistance += 2;
			currentHeight--;
			MaybeAddSpike(newTerrain.transform, TerrainType.DOWNSLOPE);
			MaybeAddCherry(newTerrain.transform);
		}
	}

	private float Distrubution(float mean, float stdDev)
	{
		float u1 = Random.Range(0f, 1f);
		float u2 = Random.Range(0f, 1f);
		float randStdNormal = Mathf.Sqrt(-2.0f * Mathf.Log(u1)) *
			Mathf.Sin(2.0f * Mathf.PI * u2); //random normal(0,1)
		float randNormal =
					 mean + stdDev * randStdNormal; //random normal(mean,stdDev^2)
		return randNormal;
	}

	private void MaybeAddSpike(Transform terrain, TerrainType terrainType)
	{
		if (lastSpikeX + minSpikeCloseness > terrain.position.x)
			return;
		if (Random.Range(0f, 1f) < spikeProbability)
		{
			bool skullSpike = (Random.Range(0f, 1f) < skullProbability);
			Quaternion rotation = Quaternion.identity;
			Vector3 offset = Vector3.zero;
			if (terrainType == TerrainType.UPSLOPE)
			{
				rotation = Quaternion.Euler(new Vector3(0, 0, SPIKE_ROTATION));
				offset = new Vector3(0, UPSLOPE_SPIKE_Y, 0);
			}
			if (terrainType == TerrainType.DOWNSLOPE)
			{
				rotation = Quaternion.Euler(new Vector3(0, 0, -SPIKE_ROTATION));
				offset = new Vector3(0, DOWNSLOPE_SPIKE_Y, 0);
			}
			Instantiate(skullSpike ? skullSpikePrefab : spikePrefab, terrain.position + offset, rotation, terrain);
			lastSpikeX = terrain.position.x;
		}
	}

	private void MaybeAddCherry(Transform terrain)
	{
		if (Random.Range(0f, 1f) < cherryProbability)
		{
			Vector3 offset = new Vector3(0, Random.Range(minCherryHeight, maxCherryHeight), 0);
			Instantiate(cherryPrefab, terrain.position + offset, Quaternion.identity, terrain);
		}
	}
	#endregion
}
