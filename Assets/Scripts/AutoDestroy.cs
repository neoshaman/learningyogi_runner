﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour {

	#region Private Variables
	private Camera cam;
	#endregion

	#region MonoBehaviour   
	void Awake()
	{
		cam = Camera.main;
	}

	void Update () {
		//if the object is offscreen to the left, destroy it
		Vector3 viewportPoint = cam.WorldToViewportPoint(transform.position);
		if (viewportPoint.x < -.3f)
			Destroy(gameObject);
	}
	#endregion
}
